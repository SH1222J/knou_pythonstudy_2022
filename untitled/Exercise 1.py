#가로가 12cm, 세로가 34cm인 사각형의 면적을 계산하는 프로그램을 작성하시오.

def quadrangle(x,y):
    return x*y

x = int(input("사각형의 x의 길이 : "))
y = int(input("사각형의 y의 길이 : "))
result = quadrangle(x,y)
print("가로가",x,"cm, 세로가",y,"cm인 사각형의 면적 :", result)
