"""
다음과 같이 문자열을 입력받은 후 띄어쓰기 부분을 줄바꿈하는 프로
그램을 작성하시오.
문자열 입력 : Hello World!
Hello
World!
"""

a = input("문자열을 입력 : ")
a_mod = a.replace(" ", "\n")
print(a_mod)