"""
다음과 같이 각 점수를 입력받아 최종 점수를 계산하는 프로그램을
작성하시오.
중간고사 점수를 입력하세요. (100점 만점 : 30%) : 80
기말고사 점수를 입력하세요. (100점 만점 : 30%) : 90
과제 점수를 입력 (100점 만점 : 10%) : 100
출석 점수를 입력 (100점 만점 : 30%) : 95

중간고사 점수 = 80
기말고사 점수 = 90
과제 점수 = 100
출석 점수 = 95
최종 점수 = 89.5

(중간점수 * 0.3)+(기말점수 * 0.3)+(출석점수 * 0.1)+(중간점수 * 0.3)=
"""

mid_term = int(input("중간고사 점수를 입력하세요. (100점 만점 : 30%) : "))
final_term = int(input("기말고사 점수를 입력하세요. (100점 만점 : 30%) : "))
assignment = int(input("과제 점수를 입력 (100점 만점 : 10%) : "))
attendance = int(input("출석 점수를 입력 (100점 만점 : 30%) : "))
result = mid_term * 0.3 + final_term * 0.3 + assignment * 0.1 +  attendance * 0.3

print('중간고사 점수 = ', mid_term)
print('기말고사 점수 = ', final_term)
print('과제 점수 = ', assignment)
print('출석 점수 = ', attendance)
print('최종 점수 = ', result)

