
"""
정수를 입력 받은 후, 해당 정수가 양수인지, 음수인지, 아니면 0인지
를 판별하는 프로그램을 작성하시오.
정수를 입력 : 10
양수임

정수를 입력 : 0
양수x 음수x 0임.
"""

number = int(input("정수를 입력 : "))

if number > 0:
    print("양수임")
if number < 0:
    print("음수임")
if number == 0:
    print("양수 음수 둘다 아니야")
